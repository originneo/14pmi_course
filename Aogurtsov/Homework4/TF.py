
# coding: utf-8

# In[1]:


import nltk
import codecs
import string
import sys
import os
import json
#nltk.download()
import math
from nltk.corpus import stopwords
from collections import Counter
from lxml import etree
import pymorphy2
from scipy import spatial


# In[27]:


class Indexer(): 

    texts = []
    stop_words = []
    morph = None
    index = {}
    def initialize(self):
        self.morph = pymorphy2.MorphAnalyzer()
        self.stop_words = stopwords.words('russian')
        self.stop_words.extend(string.punctuation)
        self.parse_xml('parsed_texts.xml')
        if os.path.exists(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'index.json')):
            print ('Loading from json file')
            index_file = open(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'index.json'), 'r')
            self.index = json.load(index_file)
            index_file.close()
        else:
            print ('Creating new index')
            self.create_index()

    def parse_xml(self, filePath):
        utf8_parser = etree.XMLParser(encoding='utf-8')
        root = etree.fromstring(filePath.encode('utf-8'), parser=utf8_parser)
        for topic in root:       
            texts = []
            for text in topic:
                sentances = []
                for sent in text:
                    self.sentances.append(sent.text)

    def filter_and_normalize_words(self, words):
        return list(map(lambda word: self.morph.parse(word)[0].normal_form,
                        filter(lambda word: word not in self.stop_words, words)))

    def create_index(self):

        text_index = 0;
        documents_list = self.compute_tfidf()
        for doc in documents_list:
            for word in doc:
                if self.index.get(word):
                    self.index.get(word).append(text_index)
                else:
                    self.index.update({word: [text_index]})
            text_index +=1
        index_file = open(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'index.json'), 'w')
        json.dump(self.index, index_file, sort_keys=True, indent=4)

    def make_normlized_list(self):
        _stdout = sys.stdout
        null = open(os.devnull,'wb')
        sys.stdout = null
        normalized_words = []
        for text in self.texts:
            try:
                sys.stdout.write(text[0])
            except:           
                text = text[1:]
            normalized_words.append(self.filter_and_normalize_words(text.split(' ')))        
        sys.stdout = _stdout
        return normalized_words



    def get_texts(self):
        return self.texts

    def get_index(self):
        return self.index
    
    def compute_tfidf(self):
        words_list = self.make_normlized_list()
        def compute_tf(text):
            tf_text = Counter(text)
            for i in tf_text:
                tf_text[i] = tf_text[i]/float(len(tf_text))
            return tf_text

        def compute_idf(word, words_list):
            return math.log10(len(words_list)/sum([1.0 for i in words_list if word in i]))

        documents_list = []
        for text in words_list:
            tf_idf_dictionary = {}
            computed_tf = compute_tf(text)
            for word in computed_tf:
                tf_idf_dictionary[word] = computed_tf[word] * compute_idf(word, words_list)
            documents_list.append(tf_idf_dictionary)
        return documents_list


# In[28]:


ind = Indexer()
ind.initialize()
search_index = ind.get_index()

sentences = ind.get_texts()

while True:
    query = input('\nВведите запрос:')
    if query== "кузнечик":
        break
    search = ind.filter_and_normalize_words(query.split(" "))

    docs = set()  
    search_tfidf_vector = [1 for word in search] 
    for word in search:
        value = search_index.get(word)
        if (value):
            for sent_ind in search_index.get(word):
                docs.add(sent_ind)
        else:
            print('Пусто')
            continue
    
    docs_tfidf_vectors = {}
    for doc in docs:
        doc_vector = []
        for word in search:
            if doc in search_index.get(word):
                doc_vector.append(1)
            else:
                doc_vector.append(0)
        docs_tfidf_vectors.update({doc: doc_vector})

    cosine_vector = {}
    for doc_id, doc_vector in docs_tfidf_vectors.items():
        cosine_vector.update({doc_id: spatial.distance.cosine(doc_vector, search_tfidf_vector)})
    sort = sorted(cosine_vector.items())
    result = sort[:5]
    for doc in result:
        print(sentences[doc[0]])




# In[ ]:


get_ipython().system('telegram-send "Опрос окончен!"')

