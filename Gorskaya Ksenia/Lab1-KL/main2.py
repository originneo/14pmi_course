import pymystem3
import re
from os import listdir
from math import *

ind=0
flag=0
dictionary = []
fwd = open('Словарь стилей танцев и музыки.txt', 'w')

kolFile=0
indFile=0
lemmatizedTexts=[]
fwh = open('Характеристики слов.txt', 'w')
fwh.write('Характеристики слов' + '\n')

for folderFile in range(2):
    if folderFile == 0:
        files = listdir('Стили танцев')
    if folderFile == 1:
        files = listdir('Стили музыки')
    for fileName in files:
        if folderFile == 0:
            fw = open('Результаты для стилей танцев.txt', 'w')
            fw.write('Стили танцев' + '\n\n')
            f=open('Стили танцев/' + fileName,'r')
        if folderFile == 1:
            fw=open('Результаты для стилей музыки.txt','w')
            fw.write('Стили музыки' + '\n\n')
            f=open('Стили музыки/' + fileName,'r')
        text=f.read()
        text=re.sub(r'[^\w\s]','', text)
        text=re.sub('\d','', text)
        text=re.sub(r'[\n\r\t]',' ', text)
        print(text)
        mystem = pymystem3.Mystem()
        lemmatizedText = mystem.lemmatize(text)
        lemmatizedTexts.append(lemmatizedText)
        indFile+=1
        print(lemmatizedText)
        textFromIndices = ""
        for i in range(len(lemmatizedText)):
            if not lemmatizedText[i].startswith(' '):
                if not lemmatizedText[i].startswith('\n'):
                    if ind > 0:
                        kol=0
                        for item in dictionary:
                            if item==lemmatizedText[i]:
                                textFromIndices = textFromIndices + str(kol) + ' '
                                flag=1
                            kol=kol+1
                    if flag==0:
                        dictionary.append(lemmatizedText[i])
                        textFromIndices = textFromIndices + str(ind) + ' '
                        ind=ind+1
                    flag=0

        print(textFromIndices)
        fw.write(fileName + '\n')
        fw.write(textFromIndices + '\n\n')
    kol=0
for item in dictionary:
    fwd.write(str(kol) + ": " + item + "\n")
    kol=kol+1
f.close()
fw.close()




print('\n\n\n')
files = listdir('Стили танцев')
indFile=0
kolFileA=0
kolFileB=0
indD=0
a=[len(dictionary)]
b=[len(dictionary)]
c=[len(dictionary)]
CCC=0
for item in dictionary:
    flag=0
    indFile=0
    for lemText in lemmatizedTexts:
        # if indFile<len(files):
        for i in range(len(lemText)):
            if not lemText[i].startswith(' '):
                if not lemText[i].startswith('\n'):
                    if lemText[i]==item:
                        if indFile < len(files):
                            kolFileA+=1
                        else:
                            kolFileB+=1
                        flag=1
                        break
        if flag==1:
            flag=0
        indFile += 1

    aA=kolFileA/len(files)
    bB=kolFileB/len(files)
    a.append(aA)
    b.append(bB)
    cC = log((aA * (1 - bB) + 0.5) / (bB * (1 - aA) + 0.5))
    c.append(cC)
    CCC += log((1 - bB + 0.5) / (1 - aA + 0.5))
    fwh.write(str(indD) + ": " + item + '  a:' + str(aA) + '  b:' + str(bB) + '  c: ' + str(round(cC,2)) + "\n")
    indD+=1
    kolFileA=0
    kolFileB=0
fwh.write('C= ' + str(CCC))
fwd.close()
fwh.close()

f = open('Input.txt', 'r')
f2 = open('Output.txt', 'w')
g=CCC
text=f.read()
text=re.sub(r'[^\w\s]','', text)
text=re.sub('\d','', text)
text=re.sub(r'[\n\r\t]',' ', text)
print(text)
mystem = pymystem3.Mystem()
lemmatizedText = mystem.lemmatize(text)
print(lemmatizedText)
for i in range(len(lemmatizedText)):
    if not lemmatizedText[i].startswith(' '):
        if not lemmatizedText[i].startswith('\n'):
            indD=0
            for item in dictionary:
                if lemmatizedText[i]==item:
                    g+=c[indD]
                    break
                indD+=1
f2.write("g=" + str(g))