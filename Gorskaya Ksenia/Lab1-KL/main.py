import pymystem3
import re
from os import listdir

ind=0
flag=0
dictionary = []
fwd = open('Словарь стилей танцев и музыки.txt', 'w')
for folderFile in range(2):
    if folderFile == 0:
        files = listdir('Стили танцев')
    if folderFile == 1:
        files = listdir('Стили музыки')
    for fileName in files:
        if folderFile == 0:
            fw = open('Результаты для стилей танцев.txt', 'w')
            fw.write('Стили танцев' + '\n\n')
            f=open('Стили танцев/' + fileName,'r')
        if folderFile == 1:
            fw=open('Результаты для стилей музыки.txt','w')
            fw.write('Стили музыки' + '\n\n')
            f=open('Стили музыки/' + fileName,'r')
        text=f.read()
        text=re.sub(r'[^\w\s]','', text)
        text=re.sub('\d','', text)
        text=re.sub(r'[\n\r\t]',' ', text)
        print(text)
        mystem = pymystem3.Mystem()
        lemmatizedText = mystem.lemmatize(text)
        print(lemmatizedText)
        textFromIndices = ""
        for i in range(len(lemmatizedText)):
            if not lemmatizedText[i].startswith(' '):
                if not lemmatizedText[i].startswith('\n'):
                    if ind > 0:
                        kol=0
                        for item in dictionary:
                            if item==lemmatizedText[i]:
                                textFromIndices = textFromIndices + str(kol) + ' '
                                flag=1
                            kol=kol+1
                    if flag==0:
                        dictionary.append(lemmatizedText[i])
                        textFromIndices = textFromIndices + str(ind) + ' '
                        ind=ind+1
                    flag=0

        print(textFromIndices)
        fw.write(fileName + '\n')
        fw.write(textFromIndices + '\n\n')
    kol=0
for item in dictionary:
    fwd.write(str(kol) + ": " + item + "\n")
    kol=kol+1
f.close()
fw.close()
fwd.close()