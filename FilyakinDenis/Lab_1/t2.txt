I remind myself that Kate has been to the best private schools in Washington. Her family has money, and she's grown up confident and sure of her place in the world. She doesn't take any crap. I am in awe of her.

"Thank you for taking the time to do this." She gives him a polite, professional smile.

"It's a pleasure," he answers, turning his gray gaze on me, and I flush, again. Damn it.

"This is Jose Rodriguez, our photographer," I say, grinning at Jose who smiles with affection back at me. His eyes cool when he looks from me to Grey.

"Mr. Grey," he nods.

"Mr. Rodriguez," Grey's expression changes too as he appraises Jose.

"Where would you like me?" Grey asks him. His tone sounds vaguely threatening. But Katherine is not about to let Jose run the show.

"Mr. Grey - if you could sit here, pleaseBe careful of the lighting cables. And then we'll do a few standing, too." She directs him to a chair set up against the wall.

Travis switches on the lights, momentarily blinding Grey, and mutters an apology.

Then Travis and I stand back and watch as Jose proceeds to snap away. He takes several photographs hand-held, asking Grey to turn this way, then that, to move his arm, then put it down again. Moving to the tripod, Jose takes several more, while Grey sits and poses, patiently and naturally, for about twenty minutes. My wish has come true: I can stand and admire Grey from not-so-afar. Twice our eyes lock, and I have to tear myself away from his cloudy gaze.

"Enough sitting." Katherine wades in again. "Standing, Mr. Grey?" she asks.

He stands, and Travis scurries in to remove the chair. The shutter on Jose's Nikon starts clicking again.

"I think we have enough," Jose announces five minutes later.

"Great," says Kate. "Thank you again, Mr. Grey." She shakes his hand, as does Jose.

"I look forward to reading the article, Miss Kavanagh," murmurs Grey, and turns to me, standing by the door. "Will you walk with me, Miss Steele?" he asks.

"Sure," I say, completely thrown. I glance anxiously at Kate, who shrugs at me. I notice Jose scowling behind her.

"Good day to you all," says Grey as he opens the door, standing aside to allow me out first.

Holy hell... what's this aboutWhat does he want I pause in the hotel corridor, fidgeting nervously as Grey emerges from the room followed by Mr. Buzz-Cut in his sharp suit.

"I'll call you, Taylor," he murmurs to Buzz-Cut. Taylor wanders back down the corridor, and Grey turns his burning gray gaze to me. Crap... have I done something wrong?

"I wondered if you would join me for coffee this morning."

My heart slams into my mouth. A dateChristian Grey is asking me on a date. He's asking if you want a coffee. Maybe he thinks you haven't woken up yet, my subconscious whines at me in a sneering mood again. I clear my throat trying to control my nerves.

"I have to drive everyone home," I murmur apologetically, twisting my hands and fingers in front of me.

"TAYLOR," he calls, making me jump. Taylor, who had been retreating down the corridor, turns and heads back toward us.

"Are they based at the university?" Grey asks, his voice soft and inquiring. I nod, too stunned to speak.

"Taylor can take them. He's my driver. We have a large 4x4 here, so he'll be able to take the equipment too."

"Mr. Grey?" Taylor asks when he reaches us, giving nothing away.

"Please, can you drive the photographer, his assistant, and Miss Kavanagh back home?"

"Certainly, sir," Taylor replies.

"There. Now can you join me for coffee?" Grey smiles as if it's a done deal.

I frown at him.

"Um - Mr. Grey, err - this really... look, Taylor doesn't have to drive them home." I flash a brief look at Taylor, who remains stoically impassive. "I'll swap vehicles with Kate, if you give me a moment."

Grey smiles a dazzling, unguarded, natural, all-teeth-showing, glorious smile. Oh my... and he opens the door of the suite so I can re-enter. I scoot around him to enter the room, finding Katherine in deep discussion with Jose.

"Ana, I think he definitely likes you," she says with no preamble whatsoever. Jose glares at me with disapproval. "But I don't trust him," she adds. I raise my hand up in the hope that she'll stop talking. By some miracle, she does.

"Kate, if you take the Beetle, can I take your car?"

"Why?"

"Christian Grey has asked me to go for coffee with him."

Her mouth pops open. Speechless Kate! I savor the moment. She grabs me by my arm and drags me into the bedroom that's off the living area of the suite.

"Ana, there's something about him." Her tone is full of warning. "He's gorgeous, I agree, but I think he's dangerous. Especially to someone like you."

"What do you mean, someone like me?" I demand, affronted.

"An innocent like you, Ana. You know what I mean," she says a little irritated. I flush.

"Kate, it's just coffee. I'm starting my exams this week, and I need to study, so I won't be long."

She purses her lips as if considering my request. Finally, she fishes her car keys out of her pocket and hands them to me. I hand her mine.

"I'll see you later. Don't be long, or I'll send out search and rescue."

"Thanks." I hug her.

I emerge from the suite to find Christian Grey waiting, leaning up against the wall, looking like a male model in a pose for some glossy high-end magazine.

"Okay, let's do coffee," I murmur, flushing a beet red.